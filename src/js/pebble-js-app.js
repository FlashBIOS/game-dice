Pebble.addEventListener("ready", function(e) {
	console.log("JavaScript app ready and running!");
});

Pebble.addEventListener("showConfiguration", function(e) {
	console.log("Open configuration.");
	Pebble.openURL('http://gamemore.com/gamedice/configuration.html');
});

Pebble.addEventListener("webviewclosed", function(e) {
	console.log("Configuration window returned: " + e.response);
	var options = JSON.parse(decodeURIComponent(e.response));
	console.log("Options = " + JSON.stringify(options));

	var transactionId = Pebble.sendAppMessage( options,
		function(e) {
			console.log("Successfully delivered message with transactionId="
				+ e.data.transactionId);
		},
		function(e) {
			console.log("Unable to deliver message with transactionId="
				+ e.data.transactionId
				+ " Error is: " + e.error.message);
		}
	);
});

Pebble.addEventListener("appmessage", function(e) {
	console.log("Received message: " + e.payload);
});