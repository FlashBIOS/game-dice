#define LARGE_FONT FONT_KEY_ROBOTO_BOLD_SUBSET_49
#define MEDIUM_FONT FONT_KEY_BITHAM_30_BLACK
#define SMALL_FONT FONT_KEY_GOTHIC_18
#define SMALL_FONT_BOLD FONT_KEY_GOTHIC_18_BOLD
#define ANIMATION_SPEED 600

enum {
	NORMAL_SIDES_PKEY = 0,
	NORMAL_QUANTITY_PKEY,
	SPECIAL_SIDES_PKEY,
	SPECIAL_QUANTITY_PKEY,
	RESULT_PKEY,
	RESULT_LAYER_Y_POS_PKEY,
	MULTIPLE_RESULTS_PKEY,
	FIRST_RUN_PKEY,	
	SHAKE_ROLL_PKEY,
};

enum {
	QUANTITY_JSKEY = 10,
	SIDES_JSKEY,
	SHAKE_ROLL_JSKEY,
};

typedef struct {
	uint8_t quantity;
	uint8_t sides;
	bool is_special;
} Dice;

static Window *window;
static NumberWindow *number_window_sides;
static NumberWindow *number_window_quantity;
static TextLayer *result_layer; // Shows the number result of the die roll
static TextLayer *multiple_results_layer; // Shows the individual number result of the die roll
static TextLayer *dice_normal_layer; // Shows the information about the future roll
static TextLayer *label_dice_normal_layer; // Shows the label for the dice normal layer text
static Dice dice_normal;
static Dice dice_special;
static char dice_normal_layer_text[10];
static char result_layer_text[5];
static char multiple_results_layer_text[510];
static char number_window_label_text[15];
static PropertyAnimation *prop_animation;
static PropertyAnimation *prop_animation_bounce;
static int result_layer_y_pos;
static bool enable_shake_roll;

static void set_result_layer_text(uint16_t sum, uint8_t roll_results[], Dice d);
static void begin_dice_roll(Dice *d);
static void animation_bounce(Animation *animation, bool finished, void *data);
static void animation_stopped(Animation *animation, bool finished, void *data);
static void destroy_property_animation(PropertyAnimation **prop_animation);
static void set_number_window_title(NumberWindow *numberwindow, uint8_t quantity, uint8_t sides);