#include <pebble.h>
#include "GameDice.h"

static void roll_dice(Dice d) {
	uint16_t sum = 0;
	uint8_t roll_results[d.quantity];

	for(uint8_t i = 0; i < d.quantity; i++)
	{
		uint8_t rand_num = rand() % d.sides + 1;
		sum += rand_num;
		roll_results[i] = rand_num;
	}
	set_result_layer_text(sum, roll_results, d);
}

static void set_result_layer_text(uint16_t sum, uint8_t roll_results[], Dice d) {
	snprintf(result_layer_text, sizeof(result_layer_text), "%d", sum);
	text_layer_set_text(result_layer, result_layer_text);
	
	strcpy(multiple_results_layer_text, ""); // We don't want the results from the previous roll
	
	if(d.is_special) {
		char special_text[20] = "";
		snprintf(special_text, sizeof(special_text), "Quick %dd%d Roll ", d.quantity, d.sides);
		strcpy(multiple_results_layer_text, special_text);
	}
	else if(d.quantity > 1)
	{	
		for(uint8_t i = 0; i < d.quantity; i++) {
			char number[4];
			snprintf(number, sizeof(number), "%d", roll_results[i]);
			if(i > 0)
				strcat(multiple_results_layer_text, ", ");
			strcat(multiple_results_layer_text, number);
		}
	}
		
	text_layer_set_text(multiple_results_layer, multiple_results_layer_text);
}

static void select_click_handler(ClickRecognizerRef recognizer, void *context) {
	//set_number_window_title(number_window_quantity, 0, dice_normal.sides);
	snprintf(number_window_label_text, sizeof(number_window_label_text), "Set Quantity");
	window_stack_push((Window*)number_window_quantity, true);
}

static void up_click_handler(ClickRecognizerRef recognizer, void *context) {
	begin_dice_roll(&dice_special);
}

static void down_click_handler(ClickRecognizerRef recognizer, void *context) {
	begin_dice_roll(&dice_normal);
}

static void up_long_click_handler(ClickRecognizerRef recognizer, void *context) {
	vibes_short_pulse();
	dice_special = dice_normal;
	dice_special.is_special = true;

	strcpy(result_layer_text, "");
	snprintf(multiple_results_layer_text, sizeof(multiple_results_layer_text), "Quick %dd%d Roll Set", dice_special.quantity, dice_special.sides);
	
	text_layer_set_text(result_layer, result_layer_text);
	text_layer_set_text(multiple_results_layer, multiple_results_layer_text);
}

static void up_long_click_release_handler(ClickRecognizerRef recognizer, void *context) {
}

static void click_config_provider(void *context) {
	window_single_click_subscribe(BUTTON_ID_SELECT, select_click_handler);
	window_single_click_subscribe(BUTTON_ID_UP, up_click_handler);
	window_single_click_subscribe(BUTTON_ID_DOWN, down_click_handler);
	window_long_click_subscribe(BUTTON_ID_UP, 2000, up_long_click_handler, up_long_click_release_handler);
}

void accel_tap_handler(AccelAxisType axis, int32_t direction) {
	if(enable_shake_roll) {
		vibes_short_pulse();
		begin_dice_roll(&dice_normal);
	}
}

static void set_dice_normal_layer_text() {
	snprintf(dice_normal_layer_text, sizeof(dice_normal_layer_text), "%dd%d", dice_normal.quantity, dice_normal.sides);
	text_layer_set_text(dice_normal_layer, dice_normal_layer_text);
}

static void begin_dice_roll(Dice *d) {
	Layer *layer = text_layer_get_layer(result_layer);
	GRect to_rect = layer_get_frame(layer);

	to_rect.origin.y = 113;

	destroy_property_animation(&prop_animation);
	prop_animation = property_animation_create_layer_frame(layer, NULL, &to_rect);
	animation_set_curve((Animation*) prop_animation, AnimationCurveEaseInOut);
	animation_set_duration(&prop_animation->animation, ANIMATION_SPEED);
	animation_set_handlers((Animation*) prop_animation, (AnimationHandlers) { 
		.stopped = (AnimationStoppedHandler) animation_bounce,
	}, d /* callback data */);
	animation_schedule((Animation *)prop_animation);
}

static void animation_bounce(Animation *animation, bool finished, void *data) {
	Layer *layer = text_layer_get_layer(result_layer);
	GRect to_rect = layer_get_frame(layer);
	
	roll_dice(*((Dice *)data));
	
	GSize result_text_size = text_layer_get_content_size(result_layer);
	GSize multiple_results_text_size = text_layer_get_content_size(multiple_results_layer);
	
	result_layer_y_pos = (90 - result_text_size.h - multiple_results_text_size.h) / 2 - 8; // For the bounce;
	to_rect.origin.y = result_layer_y_pos;

	destroy_property_animation(&prop_animation);
	prop_animation = property_animation_create_layer_frame(layer, NULL, &to_rect);
	animation_set_curve((Animation*) prop_animation, AnimationCurveEaseOut);
	animation_set_duration(&prop_animation->animation, ANIMATION_SPEED);
	animation_set_handlers((Animation*) prop_animation, (AnimationHandlers) { 
		.stopped = (AnimationStoppedHandler) animation_stopped,
	}, NULL /* callback data */);
	animation_schedule((Animation *)prop_animation);
}

static void animation_stopped(Animation *animation, bool finished, void *data) {
	Layer *layer = text_layer_get_layer(result_layer);
	GRect to_rect = layer_get_frame(layer);
		
	GSize result_text_size = text_layer_get_content_size(result_layer);
	GSize multiple_results_text_size = text_layer_get_content_size(multiple_results_layer);
	
	result_layer_y_pos = (90 - result_text_size.h - multiple_results_text_size.h) / 2;
	to_rect.origin.y = result_layer_y_pos;

	destroy_property_animation(&prop_animation_bounce);
	prop_animation_bounce = property_animation_create_layer_frame(layer, NULL, &to_rect);
	animation_set_curve((Animation*) prop_animation_bounce, AnimationCurveEaseInOut);
	animation_set_duration(&prop_animation_bounce->animation, ANIMATION_SPEED/2);
	animation_schedule((Animation*) prop_animation_bounce);
}

static void destroy_property_animation(PropertyAnimation **prop_animation) {
	if (*prop_animation == NULL) {
		return;
	}

	if (animation_is_scheduled((Animation*) *prop_animation)) {
		animation_unschedule((Animation*) *prop_animation);
	}

	property_animation_destroy(*prop_animation);
	*prop_animation = NULL;
}

static void window_load(Window *window) {
	Layer *window_layer = window_get_root_layer(window);
	GRect bounds = layer_get_bounds(window_layer);
	
	// result_layer	
	result_layer = text_layer_create((GRect) { .origin = { 0, result_layer_y_pos }, .size = { bounds.size.w, 103 } });
	text_layer_set_text(result_layer, result_layer_text);
	text_layer_set_font(result_layer, fonts_get_system_font(LARGE_FONT));
	text_layer_set_text_alignment(result_layer, GTextAlignmentCenter);
	text_layer_set_background_color(result_layer, GColorClear);
	layer_add_child(window_layer, text_layer_get_layer(result_layer));

	// multiple_results_layer
	multiple_results_layer = text_layer_create((GRect) { .origin = { 0, 48 }, .size = { bounds.size.w, 55 } });
	text_layer_set_text(multiple_results_layer, multiple_results_layer_text);
	text_layer_set_font(multiple_results_layer, fonts_get_system_font(SMALL_FONT_BOLD));
	text_layer_set_overflow_mode(multiple_results_layer, GTextOverflowModeFill);
	text_layer_set_text_alignment(multiple_results_layer, GTextAlignmentCenter);
	text_layer_set_background_color(multiple_results_layer, GColorClear);
	layer_add_child((Layer *)result_layer, text_layer_get_layer(multiple_results_layer));
	
	// label_dice_normal_layer	
	label_dice_normal_layer = text_layer_create((GRect) { .origin = { 0, 102 }, .size = { bounds.size.w, 50 } });
	text_layer_set_text(label_dice_normal_layer, "Rolling:");
	text_layer_set_font(label_dice_normal_layer, fonts_get_system_font(SMALL_FONT));
	text_layer_set_text_alignment(label_dice_normal_layer, GTextAlignmentCenter);
	text_layer_set_text_color(label_dice_normal_layer, GColorWhite);
	text_layer_set_background_color(label_dice_normal_layer, GColorBlack);
	layer_add_child(window_layer, text_layer_get_layer(label_dice_normal_layer));
	
	// dice_normal_layer	
	dice_normal_layer = text_layer_create((GRect) { .origin = { 0, 117 }, .size = { bounds.size.w, 35 } });
	set_dice_normal_layer_text();
	text_layer_set_font(dice_normal_layer, fonts_get_system_font(MEDIUM_FONT));
	text_layer_set_text_alignment(dice_normal_layer, GTextAlignmentCenter);
	text_layer_set_text_color(dice_normal_layer, GColorWhite);
	text_layer_set_background_color(dice_normal_layer, GColorClear);
	layer_add_child(window_layer, text_layer_get_layer(dice_normal_layer));
}

static void window_unload(Window *window) {
	text_layer_destroy(multiple_results_layer);
	text_layer_destroy(result_layer);
	text_layer_destroy(label_dice_normal_layer);
	text_layer_destroy(dice_normal_layer);
}

static void number_window_sides_selected(NumberWindow *numberwindow, void *context) {
	set_dice_normal_layer_text();
	window_stack_pop(true);
	begin_dice_roll(&dice_normal);
}

static void number_window_sides_incremented(NumberWindow *numberwindow, void *context) {
	uint8_t number_value = number_window_get_value(numberwindow);
	//APP_LOG(APP_LOG_LEVEL_DEBUG, "number_value: %d", number_value);
	if(number_value <= 3)
		number_window_set_value(numberwindow, 3);
	else if(number_value <= 4)
		number_window_set_value(numberwindow, 4);
	else if(number_value <= 6)
		number_window_set_value(numberwindow, 6);
	else if(number_value <= 8)
		number_window_set_value(numberwindow, 8);
	else if(number_value <= 10)
		number_window_set_value(numberwindow, 10);
	else if(number_value <= 12)
		number_window_set_value(numberwindow, 12);
	else if(number_value <= 20)
		number_window_set_value(numberwindow, 20);
	else
		number_window_set_value(numberwindow, 100);
		
	dice_normal.sides = number_window_get_value(numberwindow);
	//APP_LOG(APP_LOG_LEVEL_DEBUG, "dice_normal.sides: %d", dice_normal.sides);
	
	set_number_window_title(numberwindow, dice_normal.quantity, 0);
}

static void number_window_sides_decremented(NumberWindow *numberwindow, void *context) {
	uint8_t number_value = number_window_get_value(numberwindow);
	//APP_LOG(APP_LOG_LEVEL_DEBUG, "number_value: %d", number_value);
	if(number_value <= 3)
		number_window_set_value(numberwindow, 3);
	else if(number_value <= 4)
		number_window_set_value(numberwindow, 3);
	else if(number_value <= 6)
		number_window_set_value(numberwindow, 4);
	else if(number_value <= 8)
		number_window_set_value(numberwindow, 6);
	else if(number_value <= 10)
		number_window_set_value(numberwindow, 8);
	else if(number_value <= 12)
		number_window_set_value(numberwindow, 10);
	else if(number_value <= 20)
		number_window_set_value(numberwindow, 12);
	else
		number_window_set_value(numberwindow, 20);
	
	dice_normal.sides = number_window_get_value(numberwindow);
	//APP_LOG(APP_LOG_LEVEL_DEBUG, "dice_normal.sides: %d", dice_normal.sides);
	
	set_number_window_title(numberwindow, dice_normal.quantity, 0);
}

static void number_window_quantity_selected(NumberWindow *numberwindow, void *context) {
	set_dice_normal_layer_text();
	//set_number_window_title(numberwindow, dice_normal.quantity, 0);
	snprintf(number_window_label_text, sizeof(number_window_label_text), "Set Sides");
	window_stack_pop(true);
	window_stack_push((Window*)number_window_sides, true);
}

static void number_window_quantity_incremented(NumberWindow *numberwindow, void *context) {
	uint8_t number_value = number_window_get_value(numberwindow);
	
	if(number_value > 99) {
		number_window_set_value(numberwindow, 99);
		number_value = 99;
	}
	dice_normal.quantity = number_value;
	
	set_number_window_title(numberwindow, 0, dice_normal.sides);
}

static void number_window_quantity_decremented(NumberWindow *numberwindow, void *context) {
	uint8_t number_value = number_window_get_value(numberwindow);
	
	if(number_value < 1){
		number_window_set_value(numberwindow, 1);
		number_value = 1;
	}

	dice_normal.quantity = number_value;
	
	set_number_window_title(numberwindow, 0, dice_normal.sides);
}

static void set_number_window_title(NumberWindow *numberwindow, uint8_t quantity, uint8_t sides) {
	if(quantity == 0)
		snprintf(number_window_label_text, sizeof(number_window_label_text), "Rolling: #d%d", sides);
	else if(sides == 0)
		snprintf(number_window_label_text, sizeof(number_window_label_text), "Rolling: %dd#", quantity);
	else
		snprintf(number_window_label_text, sizeof(number_window_label_text), "Rolling: %dd%d", quantity, sides);
	
	number_window_set_label(numberwindow, number_window_label_text);
}

static void out_sent_handler(DictionaryIterator *sent, void *context) {
	// outgoing message was delivered
}


static void out_failed_handler(DictionaryIterator *failed, AppMessageResult reason, void *context) {
	// outgoing message failed
}


static void in_received_handler(DictionaryIterator *received, void *context) {
	// incoming message received
	Tuple *quantity_tuple = dict_find(received, QUANTITY_JSKEY);
	Tuple *sides_tuple = dict_find(received, SIDES_JSKEY);
	Tuple *shake_roll_tuple = dict_find(received, SHAKE_ROLL_JSKEY);

	if (quantity_tuple) {
		APP_LOG(APP_LOG_LEVEL_DEBUG, "QUANTITY_JSKEY: %i", quantity_tuple->value->uint8);
		dice_special.quantity = quantity_tuple->value->uint8;
	}
	if (sides_tuple) {
		APP_LOG(APP_LOG_LEVEL_DEBUG, "SIDES_JSKEY: %i", sides_tuple->value->uint8);
		dice_special.sides = sides_tuple->value->uint8;
	}
	if (shake_roll_tuple) {
		APP_LOG(APP_LOG_LEVEL_DEBUG, "SHAKE_ROLL_JSKEY: %i", shake_roll_tuple->value->uint8);
		enable_shake_roll = (shake_roll_tuple->value->uint8 == 1) ? true : false;
	}

	set_dice_normal_layer_text();
}


static void in_dropped_handler(AppMessageResult reason, void *context) {
	// incoming message dropped
}

// bool is_first_run() {
// 	if(!persist_exists(FIRST_RUN_PKEY)) {
// 		APP_LOG(APP_LOG_LEVEL_DEBUG, "This is the first run.");
// 		persist_write_int(FIRST_RUN_PKEY, 20);
// 		return true;
// 	}
// 	else if(persist_read_int(FIRST_RUN_PKEY) != 20){
// 		APP_LOG(APP_LOG_LEVEL_DEBUG, "This is the first run. 3");
// 		persist_write_int(FIRST_RUN_PKEY, 20);
// 		return true;
// 	}
// 	
// 	
// 	
// 	return false;
// }

static void init(void) {
	// if(is_first_run())
	// 	APP_LOG(APP_LOG_LEVEL_DEBUG, "First run value: %ld", persist_read_int(FIRST_RUN_PKEY));
	// else
	// 	APP_LOG(APP_LOG_LEVEL_DEBUG, "Not first run. Value: %ld", persist_read_int(FIRST_RUN_PKEY));
		
	srand(time(NULL));

	dice_normal.quantity = persist_exists(NORMAL_QUANTITY_PKEY) ? persist_read_int(NORMAL_QUANTITY_PKEY) : 1;		
	dice_normal.sides = persist_exists(NORMAL_SIDES_PKEY) ? persist_read_int(NORMAL_SIDES_PKEY) : 20;
	dice_normal.is_special = false;
	dice_special.quantity = persist_exists(SPECIAL_QUANTITY_PKEY) ? persist_read_int(SPECIAL_QUANTITY_PKEY) : 1;
	dice_special.sides = persist_exists(SPECIAL_SIDES_PKEY) ? persist_read_int(SPECIAL_SIDES_PKEY) : 20;
	dice_special.is_special = true;
	enable_shake_roll = persist_exists(SHAKE_ROLL_PKEY) ? persist_read_bool(SHAKE_ROLL_PKEY) : true;
	
	if(persist_exists(RESULT_PKEY))
		persist_read_string(RESULT_PKEY, result_layer_text, sizeof(result_layer_text));
	else
		snprintf(result_layer_text, sizeof(result_layer_text), "%d", 0);
	result_layer_y_pos = persist_exists(RESULT_LAYER_Y_POS_PKEY) ? persist_read_int(RESULT_LAYER_Y_POS_PKEY) : 20;
	if(persist_exists(MULTIPLE_RESULTS_PKEY))
		persist_read_string(MULTIPLE_RESULTS_PKEY, multiple_results_layer_text, sizeof(multiple_results_layer_text));
	
	accel_tap_service_subscribe(&accel_tap_handler);

	window = window_create();
	window_set_click_config_provider(window, click_config_provider);
	window_set_window_handlers(window, (WindowHandlers) {
		.load = window_load,
		.unload = window_unload,
	});

	number_window_sides = number_window_create(number_window_label_text, (NumberWindowCallbacks) {
		.incremented = number_window_sides_incremented,
		.decremented = number_window_sides_decremented,
		.selected = number_window_sides_selected,
	}, NULL);
	number_window_set_value(number_window_sides, dice_normal.sides);
	
	number_window_quantity = number_window_create(number_window_label_text, (NumberWindowCallbacks) {
		.incremented = number_window_quantity_incremented,
		.decremented = number_window_quantity_decremented,
		.selected = number_window_quantity_selected,
	}, NULL);
	number_window_set_value(number_window_quantity, dice_normal.quantity);
	
	set_number_window_title(number_window_sides, dice_normal.quantity, dice_normal.sides);
	set_number_window_title(number_window_quantity, dice_normal.quantity, dice_normal.sides);
	
	app_message_register_inbox_received(in_received_handler);
	app_message_register_inbox_dropped(in_dropped_handler);
	app_message_register_outbox_sent(out_sent_handler);
	app_message_register_outbox_failed(out_failed_handler);

	const uint32_t inbound_size = 64;
	const uint32_t outbound_size = 64;
	app_message_open(inbound_size, outbound_size);

	window_stack_push(window, true);
}

static void deinit(void) {
	accel_tap_service_unsubscribe();

	destroy_property_animation(&prop_animation);
	destroy_property_animation(&prop_animation_bounce);

	persist_write_int(NORMAL_SIDES_PKEY, dice_normal.sides);
	persist_write_int(NORMAL_QUANTITY_PKEY, dice_normal.quantity);
	persist_write_int(SPECIAL_SIDES_PKEY, dice_special.sides);
	persist_write_int(SPECIAL_QUANTITY_PKEY, dice_special.quantity);
	persist_write_int(RESULT_LAYER_Y_POS_PKEY, result_layer_y_pos);
	persist_write_string(RESULT_PKEY, result_layer_text);
	persist_write_string(MULTIPLE_RESULTS_PKEY, multiple_results_layer_text);
	persist_write_bool(SHAKE_ROLL_PKEY, enable_shake_roll);

	window_destroy(window);
}

int main(void) {
	init();

	//APP_LOG(APP_LOG_LEVEL_DEBUG, "Done initializing, pushed window: %p", window);

	app_event_loop();
	deinit();
}
